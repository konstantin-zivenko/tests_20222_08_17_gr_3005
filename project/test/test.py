import unittest
from fractions import Fraction

from project.my_sum.func import sum

class TestSum(unittest.TestCase):
    def test_list_int(self):
        data = [1, 2, 3]
        result = sum(data)
        self.assertEqual(result, 6)
        # self.assertEqual(a, b)
        # self.assertTrue(x)
        # self.assertFalse(y)
        # self.assertIs(a, b)    a is b

    def test_tuple_int(self):
        data = (1, 2, 3)
        result = sum(data)
        self.assertEqual(result, 6)

    def test_list_float(self):
        data = [1.5, 2.5, 3.5]
        result = sum(data)
        self.assertEqual(result, 7.5)

    def test_negative_int(self):
        data = [1, -2, 3]
        result = sum(data)
        self.assertEqual(result, 2)

    def test_list_fraction(self):
        data = [Fraction(1, 4), Fraction(1, 4), Fraction(2, 5)]
        result = sum(data)
        self.assertEqual(result, 1)



if __name__ == "__main__":
    unittest.main()



